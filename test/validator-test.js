//On importe chai
const chai = require('chai')
    //On importe "expect" assertion de Chai
const expect = chai.expect
    //On importe notre fichier où est située la fonction qu'on veut tester
const validator = require('../validator.js')

describe("teste la fonction IsNumValid", () => {
    it("doit retourner True pour un nombre entre 10 et 70", () => {
        expect(validator.isNumValid("39")).to.be.true
    });
    it("doit retourner False pour un nombre plus petit que 10", () => {
        expect(validator.isNumValid("9")).to.be.false
    });
    it("doit retourner False pour un nombre plus grand que 70", () => {
        expect(validator.isNumValid("99")).to.be.false
    });
})