//Fonction qui retourne True pour un nombre entre 10 et 70
exports.isNumValid = function(num) {
    if (num > 70 || num < 10) {
        return false;
    } else {
        return true;
    }
}