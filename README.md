## Démo MochaJS et Chai dans le cadre d'une veille
*formation Développeuse Web avec Simplon - semaine 11*

- Vous devez avoir NodeJS installé

https://nodejs.org/en/download/

- Clonez le projet et installez les dépendances

```
git clone git@gitlab.com:malandel/mocha_chai_demo.git
cd mocha_chai_demo
// Installer Chai & Mocha
npm install
```

- Lancer le test dans le terminal

```
npm test
ou
mocha
```

- C'est une démonstration très simple d'un test unitaire avec NodeJS, le test unitaire dans le navigateur ne fonctionne pas pour le moment